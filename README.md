
Example project do demonstrate how easy it is to break GitLab pipelines with
MR with valid syntax


## Pipeline overview

![Pipeline structure ](./images/not-so-easy-pipeline.png)

Pipeline has 2 linter jobs and 2 build jobs. Build 1 uses needs for linter 1 to get build 1 started as soon as linter 1 has finished.

Build-docs job needs output from build-1 and build-2 to generate documentation.

# Task: add build-3 

Build-3 is slow is not needed by build-docs job. 
Dependencies: can be replaced with needs: to get build-docs running when build-1 and build-2 are done. 

```
build-docs:
  # use needs to make build-docs job start faster
  needs:
    - build-1
    - build-2

```

Add new job:

```
build-3:
  stage: build
  script:
    - echo "this job is really long and blocks build stage for a long time"
    - sleep 60
```

Make an MR that adds build-3, modifies build-docs and includes a change to common-code to get build-docs change tested.
Pipeline for MR will look like this:
![Pipeline structure ](./images/not-so-easy-pipeline-build-3.png)
